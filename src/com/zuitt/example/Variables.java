package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        int age;
        char middleName;

        // Variable Declaration vs Initialization
        int x;
        int y = 0;

//        Initialization after declaration
        x = 1;

//        Output to the systems
        System.out.println("The value of y is " + y + " and the value of x is " + x);

//        Primitive Data Types
//        predefined within the Java programming language which is used for single-valued variable with limited capabilities

//        int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

//        long
//        L is being added at the end of the long number to be recognized.
        long worldPopulation = 78721312312312L;
        System.out.println(worldPopulation);

//        float
//        add f at the end of the float to be recognized
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

//       double - floating point values
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        char letter = 'a';
        System.out.println(letter);

//        boolean - true or false;
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

//        constants
//        Java uses the "final" keyword so the variable's value cannot be changed.
        final int PRINCIPAL = 30000;
        System.out.println(PRINCIPAL);
//        PRINCIPAL = 4000; - this will result to an error because of final keyword or constant variable

//        Non-primitive Data

//        String
//        Stores a sequence or array of characters
//        Strings are actually object that can use methods.
        String username = "JRoda";
        System.out.println(username);

//        Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);

    }
}
