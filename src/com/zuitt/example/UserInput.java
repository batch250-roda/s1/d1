package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.print("Enter username: ");

        String username = myObj.nextLine();
        System.out.println("User is " + username);

        System.out.println("Enter a number to add");
        System.out.println("Enter the first number");
//        int num1 = Integer.parseInt(myObj.nextLine());
        int num1 = myObj.nextInt();
        System.out.println("Enter the second number");
        int num2 = myObj.nextInt();

        int total = num1 + num2;
        System.out.println(total);
        System.out.println("The sum of two number is " + (num1 + num2));

    }
}
